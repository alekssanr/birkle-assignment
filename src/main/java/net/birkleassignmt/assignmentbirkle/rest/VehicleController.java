package net.birkleassignmt.assignmentbirkle.rest;

import net.birkleassignmt.assignmentbirkle.rest.exceptions.DuplicateVinNumberException;
import net.birkleassignmt.assignmentbirkle.rest.exceptions.InvalidVehicleException;
import net.birkleassignmt.assignmentbirkle.rest.exceptions.VehicleNotFoundException;
import net.birkleassignmt.assignmentbirkle.vehiclestorage.Vehicle;
import net.birkleassignmt.assignmentbirkle.vehiclestorage.VehicleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
public class VehicleController {

    @Autowired
    VehicleRepository vehicleRepository;

    @GetMapping("/vehicle/{id}")
    Vehicle get(@PathVariable("id") long vehicleId) {
        return vehicleRepository.findById(vehicleId).orElseThrow(() -> new VehicleNotFoundException(vehicleId));
    }

    @PostMapping("/vehicle")
    Vehicle create(@RequestBody Vehicle vehicle) {
        validateVehicleDataConsistency(vehicle);
        validateVinDuplication(null, vehicle);
        vehicle.setCreationDate(new Date());
        return vehicleRepository.save(vehicle);
    }

    @PutMapping("/vehicle/{id}")
    Vehicle update(@PathVariable("id") long vehicleId, @RequestBody Vehicle vehicle) {
        validateVehicleDataConsistency(vehicle);
        validateVehicleExistence(vehicleId);
        validateVinDuplication(vehicleId, vehicle);
        if (vehicle.getId() == null) {
            vehicle.setId(vehicleId);
        }
        return vehicleRepository.save(vehicle);
    }

    @DeleteMapping("/vehicle/{id}")
    void delete(@PathVariable("id") long vehicleId) {
        validateVehicleExistence(vehicleId);
        vehicleRepository.deleteById(vehicleId);
    }

    @GetMapping("/vehicles")
    List<Vehicle> vehicleList() {
        return vehicleRepository.findAll();
    }

    private void validateVinDuplication(Long vehicleId, Vehicle vehicle) {
        Optional<Vehicle> existingVin = vehicleRepository.findByVinNumber(vehicle.getVinNumber());
        if (existingVin.isPresent() && (vehicleId == null || !existingVin.get().getId().equals(vehicleId))) {
            throw new DuplicateVinNumberException(vehicle.getVinNumber());
        }
    }

    private void validateVehicleDataConsistency(Vehicle vehicle) {
        if (!vehicle.validVehicle()) {
            throw new InvalidVehicleException();
        }
    }

    private void validateVehicleExistence(long vehicleId) {
        if (!vehicleRepository.findById(vehicleId).isPresent()) {
            throw new VehicleNotFoundException(vehicleId);
        }
    }
}
