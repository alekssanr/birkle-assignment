package net.birkleassignmt.assignmentbirkle.rest.exceptions;

public class DuplicateVinNumberException extends RuntimeException {

    public DuplicateVinNumberException(String vinNumber) {
        super("VIN number already exists " + vinNumber);
    }
}
