package net.birkleassignmt.assignmentbirkle.rest.exceptions;

public class InvalidVehicleException extends RuntimeException {

    public InvalidVehicleException() {
        super("Illegal vehicle data");
    }
}
