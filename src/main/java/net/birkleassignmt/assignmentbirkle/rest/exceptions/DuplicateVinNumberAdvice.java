package net.birkleassignmt.assignmentbirkle.rest.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class DuplicateVinNumberAdvice {
    @ResponseBody
    @ExceptionHandler(DuplicateVinNumberException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    String duplicateVinNumberHandler(DuplicateVinNumberException ex) {
        return ex.getMessage();
    }
}
