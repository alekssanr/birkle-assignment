package net.birkleassignmt.assignmentbirkle.soap.tools;

import lombok.Data;

@Data
public class ImporterServiceResponse {
    private boolean success;
    private String illegalRows = "";
}
