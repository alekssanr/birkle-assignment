package net.birkleassignmt.assignmentbirkle.soap.tools;

import net.birkleassignmt.assignmentbirkle.vehiclestorage.Vehicle;
import net.birkleassignmt.assignmentbirkle.vehiclestorage.VehicleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;

@Component
public class DBDataImporterService {

    @Autowired
    VehicleRepository vehicleRepository;

    public ImporterServiceResponse importData(String linesData) {
        ImporterServiceResponse response = new ImporterServiceResponse();
        response.setSuccess(true);
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        String lines[] = linesData.split("\\r?\\n");
        String illegalRows = "";
        for (int i = 0; i < lines.length; i++) {
            String[] data = lines[i].split(",", 8);

            try {
                Vehicle vehicle = new Vehicle(
                        data[0].trim(),
                        data[1].trim(),
                        data[2].trim(),
                        data[3].trim(),
                        data[4].trim(),
                        data[5].trim(),
                        sdf.parse(data[6].trim()),
                        data[7].trim());
                if (!vehicleRepository.findByVinNumber(vehicle.getVinNumber()).isPresent()) {
                    vehicleRepository.save(vehicle);
                } else {
                    illegalRows = addIllegalRow(illegalRows, i);
                }
            } catch (Exception e) {
                illegalRows = addIllegalRow(illegalRows, i);
            }
        }
        response.setIllegalRows(illegalRows);
        response.setSuccess(illegalRows.isEmpty());
        return response;
    }

    private String addIllegalRow(String illegalRows, int i) {
        return illegalRows + (illegalRows.length() > 0 ? ("," + i) : ("" + i));
    }
}
