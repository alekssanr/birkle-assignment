package net.birkleassignmt.assignmentbirkle.soap;

import localhost._1337.importdata.ImportDataRequest;
import localhost._1337.importdata.ImportDataResponse;
import net.birkleassignmt.assignmentbirkle.soap.tools.DBDataImporterService;
import net.birkleassignmt.assignmentbirkle.soap.tools.ImporterServiceResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
public class ImportDataEndpoint {

    private static final String NAMESPACE_URI = "http://localhost:1337/importdata";

    @Autowired
    DBDataImporterService dbDataImporter;

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "ImportDataRequest")
    @ResponsePayload
    public ImportDataResponse importData(@RequestPayload ImportDataRequest request) {

        ImportDataResponse response = new ImportDataResponse();
        String linesData = request.getData();
        ImporterServiceResponse importerServiceResponse = dbDataImporter.importData(linesData);
        response.setResult(importerServiceResponse.isSuccess());
        response.setIllegalrows(importerServiceResponse.getIllegalRows());

        return response;
    }
}
