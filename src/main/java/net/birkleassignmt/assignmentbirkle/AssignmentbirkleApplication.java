package net.birkleassignmt.assignmentbirkle;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AssignmentbirkleApplication {

    public static void main(String[] args) {
        SpringApplication.run(AssignmentbirkleApplication.class, args);
    }

}
