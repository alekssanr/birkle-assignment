package net.birkleassignmt.assignmentbirkle.vehiclestorage;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;

@Entity
@Data
public class Vehicle {

    public Vehicle(String brand, String model, String vehicleType, String registrationCountry, String registrationPlate, String vinNumber, Date creationDate, String vehicleManufactCountry) {
        this.brand = brand;
        this.model = model;
        this.vehicleType = vehicleType;
        this.registrationCountry = registrationCountry;
        this.registrationPlate = registrationPlate;
        this.vinNumber = vinNumber;
        this.creationDate = creationDate;
        this.vehicleManufactCountry = vehicleManufactCountry;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String brand;
    private String model;
    private String vehicleType;
    private String registrationCountry;
    private String registrationPlate;
    @Column(unique = true)
    private String vinNumber;
    private Date creationDate;
    private String vehicleManufactCountry;

    public Vehicle() {
    }

    public boolean validVehicle() {
        boolean response = true;
        if (this.vinNumber == null || this.vinNumber.isEmpty()) {
            response = false;
        }
        if (response && this.registrationCountry != null) {
            response = Arrays.stream(Locale.getISOCountries()).anyMatch(code -> code.equalsIgnoreCase(this.registrationCountry));
        }
        if (response && this.vehicleManufactCountry != null) {
            response = Arrays.stream(Locale.getISOCountries()).anyMatch(code -> code.equalsIgnoreCase(this.vehicleManufactCountry));
        }
        return response;
    }
}
