package net.birkleassignmt.assignmentbirkle.vehiclestorage;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface VehicleRepository extends JpaRepository<Vehicle, Long> {
    Optional<Vehicle> findByVinNumber(String vinNumber);
}
