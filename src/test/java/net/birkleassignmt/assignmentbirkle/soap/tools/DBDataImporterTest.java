package net.birkleassignmt.assignmentbirkle.soap.tools;

import net.birkleassignmt.assignmentbirkle.vehiclestorage.Vehicle;
import net.birkleassignmt.assignmentbirkle.vehiclestorage.VehicleRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class DBDataImporterTest {

    @Mock
    VehicleRepository vehicleRepository;

    @InjectMocks
    DBDataImporterService dbDataImporterService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void importDataSuccess() {
        String input = "Hyundai,Tiburon,CARGO,CN,965scg,JH4DC53855S287434,09/26/2019,RU\n" +
                "Chevrolet,G-Series G20,CARGO,CN,500hic,SCFFDEDN6CG939944,02/17/2019,IL\n" +
                "Dodge,Neon,MPV,HN,009gea,3N1CN7AP9EL277066,08/17/2019,JP";
        ImporterServiceResponse response = dbDataImporterService.importData(input);
        assertTrue(response.isSuccess());
        assertTrue(response.getIllegalRows().isEmpty());
        verify(vehicleRepository, times(3)).save(any());
    }

    @Test
    void importDataIllegalDate() {
        String input = "Hyundai,Tiburon,CARGO,CN,965scg,JH4DC53855S287434,09/26/2019,RU\n" +
                "Chevrolet,G-Series G20,CARGO,CN,500hic,SCFFDEDN6CG939944,MM/17/2019,IL\n" +
                "Dodge,Neon,MPV,HN,009gea,3N1CN7AP9EL277066,08/17/2019,JP";
        ImporterServiceResponse response = dbDataImporterService.importData(input);
        assertFalse(response.isSuccess());
        assertEquals("1", response.getIllegalRows());
        verify(vehicleRepository, times(2)).save(any());
    }

    @Test
    void importDataDuplicateVin() {
        String input = "Hyundai,Tiburon,CARGO,CN,965scg,JH4DC53855S287434,09/26/2019,RU\n" +
                "Chevrolet,G-Series G20,CARGO,CN,500hic,SCFFDEDN6CG939944,02/17/2019,IL\n" +
                "Dodge,Neon,MPV,HN,009gea,3N1CN7AP9EL277066,08/17/2019,JP";
        when(vehicleRepository.findByVinNumber("3N1CN7AP9EL277066")).thenReturn(Optional.of(new Vehicle()));
        ImporterServiceResponse response = dbDataImporterService.importData(input);
        assertFalse(response.isSuccess());
        assertEquals("2", response.getIllegalRows());
        verify(vehicleRepository, times(2)).save(any());
    }
}