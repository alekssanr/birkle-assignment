package net.birkleassignmt.assignmentbirkle.rest;

import net.birkleassignmt.assignmentbirkle.rest.exceptions.DuplicateVinNumberException;
import net.birkleassignmt.assignmentbirkle.rest.exceptions.InvalidVehicleException;
import net.birkleassignmt.assignmentbirkle.rest.exceptions.VehicleNotFoundException;
import net.birkleassignmt.assignmentbirkle.vehiclestorage.Vehicle;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
class VehicleControllerTest {

    @Autowired
    VehicleController vehicleController;

    @AfterEach
    void cleanUp() {
        vehicleController.vehicleList().forEach(vehicle -> vehicleController.delete(vehicle.getId()));
    }

    @Test
    void savingModifyingListingAndDeleting() {
        Vehicle vehicle = new Vehicle();
        vehicle.setVinNumber("1945fdr");
        Vehicle createdVehicle = vehicleController.create(vehicle);
        Assertions.assertEquals("1945fdr", createdVehicle.getVinNumber());
        List<Vehicle> vehicles = vehicleController.vehicleList();
        Assertions.assertEquals(1, vehicles.size());
        Vehicle vehicleForUpdate = new Vehicle();
        vehicleForUpdate.setVinNumber("1933fdr");
        Vehicle updatedVehicle = vehicleController.update(vehicle.getId(), vehicleForUpdate);
        assertEquals("1933fdr", updatedVehicle.getVinNumber());
        vehicleController.delete(vehicle.getId());
        vehicles = vehicleController.vehicleList();
        assertEquals(0, vehicles.size());
    }

    @Test
    void exceptionsThrownOnInvalidVehicles() {
        Vehicle vehicle = new Vehicle();
        assertThrows(InvalidVehicleException.class, () -> vehicleController.create(vehicle));
        vehicle.setVinNumber("1945fdr");
        Vehicle createdVehicle = vehicleController.create(vehicle);
        assertThrows(DuplicateVinNumberException.class, () -> vehicleController.create(vehicle));
        createdVehicle.setRegistrationCountry("11");
        assertThrows(InvalidVehicleException.class, () -> vehicleController.update(createdVehicle.getId(), createdVehicle));
    }

    @Test
    void exceptionThrownNotFoundVehicle() {
        assertThrows(VehicleNotFoundException.class, () -> vehicleController.get(1));
    }
}