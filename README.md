# README #

Homework assignment for Birkle IT

### Nice to know ###

* **Rest security**

Rest endpoints are secured with basic authentication,
 
**default user/password**: birkle/pwd 

* **Documentation links**

Swagger API documentation:
[http://localhost:1337/swagger-ui.html](URL)

WSDL location: 
[http://localhost:1337/importdata/vehicleImport.wsdl](URL)


### How do I get set up? ###

* How to run the application

`./gradlew build`  - to build the application
 
 `./gradlew bootRun`  -  to run the application
 
 * How to access REST API
 
Example query to get list of vehicles:

`curl -X GET --user birkle:pwd http://localhost:1337/vehicles`

Example query that will create new vehicle: 

`curl -X POST --user birkle:pwd http://localhost:1337/vehicle -H 'Content-type:application/json' -d '{"registrationCountry": "DE", "registrationPlate": "DE3214", "vinNumber": "DEDEDEDE231"}'`

Example query to update vehicle data:

`curl -X PUT --user birkle:pwd http://localhost:1337/vehicle/{vehicleId} -H 'Content-type:application/json' -d '{"registrationCountry": "DE", "registrationPlate":
 "DE3214", "vinNumber": "EE346"}'`
 
Example query to fetch a vehicle:
 
`curl -X GET --user birkle:pwd http://localhost:1337/vehicle/{vehicleId}`

Example query to delete a vehicle:

`curl -X DELETE --user birkle:pwd http://localhost:1337/vehicle/{vehicleId}`
 
